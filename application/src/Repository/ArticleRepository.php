<?php
/**
 * Created by PhpStorm.
 * User: arkandros
 * Date: 12/10/18
 * Time: 11:40
 */

declare(strict_types=1);
namespace App\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DateTime;
use App\Entity\Article;
use Doctrine\DBAL\Connection;

class ArticleRepository
{
    private $connection;

    private static $MAPPING = [
        'id'
        => ['method' => 'setId', 'type' => 'int'],
        'text'
        => ['method' => 'setText', 'type' => 'string'],
        'created_at' => ['method' => 'setDate', 'type' => DateTime::class]
    ];

    private $articleList;

    public function __construct(Connection $co)
    {
        $this->connection = $co;
        $this->articleList = new ArrayCollection();
        $article = new Article();
        $article->setId(1)
            ->setText("Premier Message")
            ->setCreatedAt(new DateTime());
        $this->articleList->add($article);
    }

    public function findAll(): ArrayCollection
    {
        $sqlArticleList = "SELECT * FROM articles";
        return $this->hydrateAllData($this->connection->fetchAll($sqlArticleList));
    }

    protected function hydrateAllData(array $rows): ArrayCollection
    {
        $articles = new ArrayCollection();
        foreach ($rows as $row) {
            $articles->add($this->hydrateRowData($row));
        }
        return $articles;
    }

    protected function hydrateRowData(array $row): Article
    {
        $articles = new Article();
        foreach (self::$MAPPING as $fieldName => $fieldMetadata) {
            if (array_key_exists($fieldName, $row) && method_exists($articles,
            $fieldMetadata['method'])) {
                if ($fieldMetadata['type'] == DateTime::class) {
                    call_user_func(array($articles, $fieldMetadata['method']), new
                    DateTime($row[$fieldName]));
                } else if ($fieldMetadata['type'] == 'int') {
                    call_user_func(array($articles, $fieldMetadata['method']), intval
                    ($row[$fieldName]));
                } else {
                    call_user_func(array($articles, $fieldMetadata['method']), $row
                    [$fieldName]);
                }
            }
        }
        return $articles;
    }
}