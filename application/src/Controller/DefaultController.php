<?php
namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Service\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $articleRepository;
    private $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
    * @Route("/", name="home")
    */
    public function index()
    {
        return $this->render('default/home.html.twig',[
            'articleList' => $this->articleService->findAllCleanedArticle()
        ]);
    }
}