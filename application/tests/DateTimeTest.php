<?php
/**
 * Created by PhpStorm.
 * User: arkandros
 * Date: 18/10/18
 * Time: 11:19
 */
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateTimeTest extends KernelTestCase
{
    /**
     * @test
     */
    public function shouldFormatDate() {
        $dateTime = new DateTime('2016-09-01');
        $this->assertEquals('01/09/2016', $dateTime->format('d/m/Y'));
    }

    /**
    * @test
    */
    public function shouldAddInterval() {
        $dateTime = new DateTime('2016-09-01');
        $interval = new DateInterval('P2D');
        $dateTime->add($interval);
        $this->assertEquals('03/09/2016', $dateTime->format('d/m/Y'));
    }

}
